## Plant Automation v0.01
## Coded by Febri & Sulis on June 2022
import time
import sys
import watering

## Global variable
umurTanaman = 0

## The beatiful codes in universe has been written        
        
def penampungInput():
    global umurTanaman
    
    print("\n[Tanaman Bahagia v0.01]")
    print("\n- Menyiram tanaman [1] ")
    print("- Cek umur tanaman [2]\n")
    print("- Keluar program [9] \n")
    nomerInput = int(input("Input : "))
    
    if nomerInput == 1:
        umurTanaman = umurTanaman + 1
        watering.wateringImage()
        
    elif nomerInput == 2:
        print("\nUmur Tanaman: ",umurTanaman,"x Siram")
        if umurTanaman <= 5:
            watering.tanamanKecil()
            
        elif umurTanaman > 5:
            watering.tanamanBesar()
                
    elif nomerInput == 9:
        sys.exit("Exiting..")
     
## Execution
while True:
    penampungInput()
    time.sleep(1.5)
